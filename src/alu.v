module ALU
    #(parameter DATA_WIDTH = 16)
     (input      [2:0]   op
     ,input      [(DATA_WIDTH-1):0] a
     ,input      [(DATA_WIDTH-1):0] b
     ,output                        ze
     ,output                        ne
     ,output reg [(DATA_WIDTH-1):0] out);

    assign ze = ~|out;
    assign ne = out[DATA_WIDTH - 1];
    //TODO: detect overflow

    always @(*)
    begin
        case (op)
            3'b000: out = b;
            3'b001: out = a | b;
            3'b010: out = ~(a | b);
            3'b011: out = a ^ b;
            3'b100: out = a & b;
            3'b101: out = a + b;
            3'b110: out = a - b;
            3'b111: out = b;
        endcase

        $display("ALU Out: %h = %b, %h :: %h", out, op, a, b);
    end
endmodule
