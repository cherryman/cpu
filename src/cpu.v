`include "alu.v"
`include "regs.v"
`include "pc.v"
`include "rom.v"

module CPU(clk, reset, next, mem_out, mem_write, mem_addr, mem_in);

    parameter DATA_WIDTH = 16;

    input clk;
    input reset, next;

    input [(DATA_WIDTH - 1):0] mem_out;
    output mem_write;
    output [(DATA_WIDTH - 1):0] mem_addr, mem_in;

    // Toggles
    reg WriteReg;
    reg WriteMem;

    reg PCInc;
    reg PCReset;
    wire PCLoad;

    reg MemToReg;
    //reg BufToA;
    reg BufToB;

    reg ALUToComp;
    reg JumpCheckNe;
    reg CompDoesLt;
    reg IsBranch, IsJump, IsJumpReg;

    wire ALUOutIsZero, ALUOutIsNeg;

    // Data
    wire [(DATA_WIDTH - 1):0] inst;
    wire [(DATA_WIDTH - 1):0] reg_in;
    wire [(DATA_WIDTH - 1):0] mem_in;
    wire [(DATA_WIDTH - 1):0] alu_to_reg;
    wire [(DATA_WIDTH - 1):0] alu_a, alu_b;

    wire [(DATA_WIDTH - 1):0] pc_in;

    reg [2:0] alu_op;
    reg [2:0] rf, rs, rd;

    reg [(DATA_WIDTH - 1):0] jump_addr;
    reg signed [(DATA_WIDTH - 1):0] branch_offset;
    //reg [(DATA_WIDTH - 1):0] a_buf;
    reg [(DATA_WIDTH - 1):0] b_buf;

    wire comp_out;
    wire [(DATA_WIDTH - 1):0] alu_out;
    wire [(DATA_WIDTH - 1):0] mem_out;
    wire [(DATA_WIDTH - 1):0] re0_out, re1_out;
    wire [(DATA_WIDTH - 1):0] pc_out;

    // ---- Circuits
    RegisterBox #(DATA_WIDTH) regs
        (.clk(clk), .re0(rf), .re1(rs), .wr_re(rd),
        .wr_en(WriteReg), .data_in(reg_in),
        .re0_out(re0_out), .re1_out(re1_out));

    ALU #(DATA_WIDTH) alu
        (.op(alu_op), .a(alu_a), .b(alu_b), .out(alu_out),
        .ze(ALUOutIsZero), .ne(ALUOutIsNeg));

    PC #(DATA_WIDTH) pc
        (.clk(clk), .inc(PCInc), .load(PCLoad), .reset(PCReset),
        .in(pc_in), .out(pc_out));

    ROM #(DATA_WIDTH) rom
        (.addr(pc_out), .out(inst));

    assign comp_out  = ~ALUOutIsZero && ~(CompDoesLt ^ ALUOutIsNeg);
    assign mem_in    = re1_out;
    assign mem_addr  = alu_out;
    assign mem_write = WriteMem;

    // ---- Multiplexers
    assign PCLoad     = (IsBranch || IsJump) && (JumpCheckNe ^ ALUOutIsZero);
    assign pc_in      = IsBranch ? pc_out + branch_offset
                        : (IsJumpReg ? re0_out
                        : jump_addr);

    assign alu_to_reg = ALUToComp ? {15'b0, comp_out} : alu_out;
    assign reg_in     = MemToReg  ? mem_out : alu_to_reg;
    //assign alu_a      = BufToA    ? a_buf : re0_out;
    assign alu_a      = re0_out;
    assign alu_b      = BufToB    ? b_buf : re1_out;

    // ---- States Vars
    reg [1:0] state;
    parameter [1:0] RESET = 2'd0;
    parameter [1:0] MAIN  = 2'd1;

    // ---- Clock
    always @(negedge clk)
    begin
        case (state)
            RESET:
            begin
                PCReset <= 1;

                if (next) state <= MAIN;
                else      state <= RESET;
            end

            MAIN:
            begin
                PCReset <= 0;
                PCInc   <= 1;

                if (next) state <= MAIN;
                else      state <= MAIN;

                main();
            end

            default: state <= RESET;
        endcase

        if (reset == 1) state <= RESET;
    end

    //always @(negedge clk)
    task main;
    begin
        $display("EXECUTING INSTRUCTION: %b", inst);

        // Reset control bits
        WriteReg  <= 0;
        WriteMem  <= 0;
        MemToReg  <= 0;
        ALUToComp <= 0;
        //BufToA    <= 0;
        BufToB    <= 0;
        PCReset   <= 0;
        IsBranch  <= 0;
        IsJump    <= 0;
        IsJumpReg <= 0;

        case (inst[15:14])
            2'b00:   r_inst(inst[13:0]);
            2'b01:   l_inst(inst[13:0]);
            2'b10:   s_inst(inst[13:0]);
            2'b11:   j_inst(inst[13:0]);
            default: $display("Invalid instruction type");
        endcase
    end
    endtask

    // ---- Instruction Tasks  ----
    task r_inst;
        input [13:0] in;

        begin
            $display("  R-Instruction: op = %b; (rd, rf, rs) = (%d, %d, %d)",
                    in[13:11], in[10:8], in[7:5], in[4:2]);

            WriteReg <= 1;

            alu_op       <= in[13:11];
            {rd, rf, rs} <= in[10:2];
        end
    endtask

    task l_inst;
        input [13:0] in;

        begin
            $write("  L-Instruction: ");

            WriteReg <= 1;
            rd <= in[10:8];

            case (in[13:12])
                2'b00: // Mem load
                begin
                    $display("load-word");

                    MemToReg <= 1;
                    BufToB   <= 1;

                    alu_op <= 3'b101;
                    b_buf  <= {{11{in[4]}}, in[4:0]};
                    rf     <= in[7:5];
                end

                2'b10: // Immediate load
                begin
                    $display("load-immediate");

                    BufToB <= 1;

                    if (in[11]) // Upper
                    begin
                        alu_op <= 3'b001;
                        b_buf  <= {in[7:0], 8'b0};
                        rf     <= in[10:8];
                    end
                    else // Lower
                    begin
                        alu_op <= 3'b000; // nop_b
                        b_buf  <= {8'b0, in[7:0]}; // Lower
                    end
                end

                2'b01: // Comparison
                begin
                    $display("compare");

                    ALUToComp  <= 1;
                    CompDoesLt <= ~in[11];

                    alu_op <= 3'b110;
                    {rd, rf, rs} <= in[10:2];
                end

                default: $display("  Invalid store");
            endcase
        end
    endtask

    task s_inst;
        input [13:0] in;

        begin
            $write("  S-Instruction: ");
            case (in[13:12])
                2'b00: // sw
                begin
                    $display("store word");

                    WriteMem <= 1;
                    BufToB   <= 1;

                    {rs, rf} <= in[10:5];

                    alu_op <= 3'b101;
                    b_buf  <= {{11{in[4]}}, in[4:0]};
                end

                2'b11: // lju
                begin
                    $display("load jump upper: %h", in[7:0]);
                    jump_addr[15:8] <= in[7:0];
                end
                //2'b01: // swr
                //begin
                    //reset_alu_inputs();
                    //{rs, rf} <= in[10:5];
                    //WriteMem <= 1;

                    //mem_in   <= re1_out;
                //end

                default: $display("Invalid store instruction");
            endcase

        end
    endtask

    task j_inst;
        input [13:0] in;
        begin
            alu_op <= 3'b000; // to compare

            casez (in[13:12])
                2'b1z: // Branch
                begin
                    $display("  Branch");
                    IsBranch <= 1;
                    JumpCheckNe <= in[12];

                    rs <= in[11:9];
                    branch_offset <= {{7{in[8]}}, in[8:0]};
                end

                2'b00: // Jump
                begin
                    $display("  Jump");
                    IsJump <= 1;

                    // For jump reg
                    rf <= in[10:8];
                    IsJumpReg <= in[11];

                    // For basic jump
                    // Check against zero, condition always true
                    JumpCheckNe <= 0;
                    BufToB <= 1;
                    b_buf <= 0;
                    jump_addr <= {jump_addr[15:8], in[7:0]};
                end

                2'b01: // Jump COND
                begin
                    $display("  Jump-condition");
                    IsJump <= 1;
                    JumpCheckNe <= in[11];
                    rs <= in[10:8];
                    jump_addr <= {jump_addr[15:8], in[7:0]};
                end

                default: $display("  Invalid jump");
            endcase
        end
    endtask

endmodule
