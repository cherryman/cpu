`include "ram.v"

module IO(clk, wr_en, din, addr, dout, pin_in, pin_out);
    parameter DATA_WIDTH = 16;
    parameter ADDR_WIDTH = 16;

    parameter RAM_WIDTH = 15;
    parameter INPUT_WIDTH = 4;
    parameter OUTPUT_WIDTH = 4;

    input clk, wr_en;
    input [(DATA_WIDTH - 1):0] din;
    input [(ADDR_WIDTH - 1):0] addr;
    output reg [(DATA_WIDTH - 1):0] dout;

    input [(2 ** INPUT_WIDTH) - 1:0] pin_in;
    output [(2 ** OUTPUT_WIDTH) - 1:0] pin_out;

    reg pin_out_write;
    reg ram_write;

    wire [(DATA_WIDTH - 1):0] pin_in_out;
    wire [(DATA_WIDTH - 1):0] pin_out_out;
    wire [(DATA_WIDTH - 1):0] ram_out;

    wire [(DATA_WIDTH - 1):0] pin_in_data [(2 ** INPUT_WIDTH) - 1:0];
    reg [(DATA_WIDTH - 1):0] pin_out_reg[(2 ** OUTPUT_WIDTH) - 1:0];
    RAM #(DATA_WIDTH, RAM_WIDTH) ram
        (clk, ram_write, din, addr[(RAM_WIDTH - 1):0], ram_out);

    assign pin_in_out  = pin_in_data[addr[(INPUT_WIDTH - 1):0]];
    assign pin_out_out = pin_out_reg[addr[(OUTPUT_WIDTH - 1):0]];

    // Input Pin setup
    generate
        genvar po;

        for (po = 0; po < (2 ** INPUT_WIDTH); po = po + 1)
            assign pin_in_data[po] = {15'b0, pin_in[po]};

    endgenerate

    // Output Pin setup
    generate
        genvar i;

        for (i = 0; i < (2 ** OUTPUT_WIDTH); i = i + 1)
            assign pin_out[i] = |pin_out_reg[i];
    endgenerate

    always @*
    begin
        casez (addr)
            16'b100000000000zzzz: // Input
            begin
                ram_write = 0;
                pin_out_write = 0;

                dout = pin_in_out;
            end

            16'b100000000001zzzz: // Output
            begin
                ram_write = 0;
                pin_out_write = wr_en;

                dout = pin_out_out;
            end

            16'b0zzzzzzzzzzzzzzz: // RAM
            begin
                pin_out_write = 0;
                ram_write = wr_en;

                dout = ram_out;
            end

            default:
            begin
                ram_write = 0;
                pin_out_write = 0;
            end
        endcase
    end

    always @(posedge clk)
    begin
        if (pin_out_write)
            pin_out_reg[addr[(OUTPUT_WIDTH - 1):0]] <= din;
    end

endmodule
