module PC(clk, inc, load, reset, in, out);
    parameter BITS = 16;

    input clk;
    input inc;
    input load;
    input reset;
    input      [BITS - 1:0] in;
    output reg [BITS - 1:0] out;

    always @(posedge clk) begin
        if (reset)
        begin
            out <= 0;
        end
        else if (load)
        begin
            out <= in;
        end
        else if (inc)
        begin
            out <= out + 1;
        end
    end

endmodule
