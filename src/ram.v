module RAM(clk, wr_en, data_in, addr, out);
    parameter DATA_WIDTH = 16;
    parameter ADDR_WIDTH = 15;

    input clk;
    input wr_en;
    input [(DATA_WIDTH - 1):0] data_in;
    input [(ADDR_WIDTH - 1):0] addr;

    //output [(DATA_WIDTH - 1):0] out;
    output reg [(DATA_WIDTH - 1):0] out;

    (* ram_style = "block" *)
    reg [(DATA_WIDTH - 1):0] ram[(2 ** ADDR_WIDTH) - 1:0];


    //assign out = ram[addr];
    always @(negedge clk)
        out <= ram[addr];

    always @(posedge clk)
    begin
        if (wr_en)
        begin
            $display("RAM: %h <- %h", addr, data_in);
            ram[addr] <= data_in;
        end
    end
endmodule
