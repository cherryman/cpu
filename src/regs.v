module RegisterBox
    #(parameter DATA_WIDTH = 16
     ,parameter REG_NUM    = 3)
     (input                     clk
     ,input  [(REG_NUM-1):0]    re0
     ,input  [(REG_NUM-1):0]    re1
     ,input                     wr_en
     ,input  [(REG_NUM-1):0]    wr_re
     ,input  [(DATA_WIDTH-1):0] data_in
     ,output [(DATA_WIDTH-1):0] re0_out
     ,output [(DATA_WIDTH-1):0] re1_out);

    reg [(DATA_WIDTH-1):0] registers[(2 ** REG_NUM) - 1:0];

    assign re0_out = registers[re0];
    assign re1_out = registers[re1];

    always @(posedge clk)
    begin
        if (wr_en)
        begin
            $display("REG: r%h <- %h", wr_re, data_in);
            registers[wr_re] <= data_in;
        end
    end
endmodule
