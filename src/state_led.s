li	ip, 0
li	op, 0
li	sp, 0
lui	ip, 128
li	t0, 16
add	op, t0, ip

li	t0, 0
li	t1, 1

sw	t0, 0(op)
sw	t0, 1(op)
sw	t0, 2(op)
sw	t0, 3(op)
sw	t0, 4(op)

# --- STATE 0
state0:
lju state0

li	t0, 0
li	t1, 1

# reset state leds
sw	t1, 11(op)
sw	t0, 12(op)
sw	t0, 13(op)
sw	t0, 14(op)
sw	t0, 15(op)

# iterator
li	s1, 4
add	s1, op, s1

wait0:
li	s0, 8
loop0_0:
li	ra, -1
lui	ra, -1
loop0_1:
sub	ra, ra, t1
bne	ra, loop0_1
sub	s0, s0, t1
bne	s0, loop0_0

# poll button
lw	sp, 0(ip)
bne	sp, state1

# change leds
sw	t0, 0(s1)
sub	s1, s1, t1
sw	t1, 0(s1)

slt	sp, s1, op
beq	sp, wait0

sw	t1, 4(op)
j	state0


# --- STATE 1
state1:
lju	state1

li	t0, 0
li	t1, 1

# reset state leds
sw	t1, 11(op)
sw	t1, 12(op)
sw	t0, 13(op)
sw	t1, 14(op)
sw	t1, 15(op)

sw	t0, 0(op)
sw	t0, 1(op)
sw	t0, 2(op)
sw	t0, 3(op)
sw	t0, 4(op)

li	s1, 4
add	s1, op, s1

wait1:
li	s0, 16
loop1_0:
li	ra, -1
lui	ra, -1
loop1_1:
sub	ra, ra, t1
bne	ra, loop1_1
sub	s0, s0, t1
bne	s0, loop1_0

# poll button
lw	sp, 0(ip)
bne	sp, state2

sw	t1, 0(s1)
sub	s1, s1, t1

slt	sp, s1, op
beq	sp, wait1

li	s0, 16
loop2_0:
li	ra, -1
lui	ra, -1
loop2_1:
sub	ra, ra, t1
bne	ra, loop2_1
sub	s0, s0, t1
bne	s0, loop2_0
j	state1


# --- STATE 2
state2:
lju state2

li	t0, 0
li	t1, 1

li	s1, 4
add	s1, s1, op

wait3:
li	s0, 12
loop3_0:
li	ra, -1
lui	ra, -1
loop3_1:
sub	ra, ra, t1
bne	ra, loop3_1
sub	s0, s0, t1
bne	s0, loop3_0

# poll button
lw	sp, 0(ip)
bne	sp, state0

li	t0, 0
sw	t0, 0(s1)
sub	s1, s1, t1
sw	t1, 0(s1)

sgt	t0, s1, op
bne	t0, wait3
j	wait4

wait4:
li	s0, 12
loop4_0:
li	ra, -1
lui	ra, -1
loop4_1:
sub	ra, ra, t1
bne	ra, loop4_1
sub	s0, s0, t1
bne	s0, loop4_0

# poll button
lw	sp, 0(ip)
bne	sp, state0

li	t0, 0
sw	t0, 0(s1)
add	s1, s1, t1
sw	t1, 0(s1)

li	t0, 4
add	t0, op, t0
slt	t0, s1, t0
bne	t0, wait4
j	state2
