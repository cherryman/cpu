`include "alu.v"

module Test;

    reg [2:0] op;
    reg [15:0] a;
    reg [15:0] b;
    wire ze, ne;
    wire [15:0] out;

    ALU alu(op, a, b, ze, ne, out);

    task set;
        input [15:0] i_a, i_b;

        begin
            a <= i_a;
            b <= i_b;
        end
    endtask

    task test;
        input [15:0] i_out;

        begin
            if (~(i_out == out))
            begin
                $display("caca")
            end
        end
    endtask

    initial
    begin
        op = 3'b001;
