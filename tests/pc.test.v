`include "pc.v"

module Test;

    reg clk   = 0;
    reg inc   = 0;
    reg load  = 0;
    reg reset = 0;

    reg  [15:0] in;
    wire [15:0] out;

    PC pc(clk, inc, load, reset, in, out);

    task tclk;
        begin
            #100 clk = ~clk;
            #100 clk = ~clk;
        end
    endtask

    task test;
        input reg [15:0] test_out;

        if (! (out === test_out))
        begin
            $display("Failure in PC module");
            $display("inc:    %b", inc);
            $display("load:   %b", load);
            $display("reset:  %b", reset);
            $display("in:     %b", in);
            $display("out:    %b", out);
            $display("test:   %b", test_out);
        end
    endtask

    initial
    begin
        clk = 0;
        in = 0;

        reset = 1;
        tclk();
        reset = 0;

        test(0);

        inc = 1;
        tclk(); test(1);
        tclk(); test(2);
        tclk(); test(3);
        inc = 0;

        load = 1;
        in = 65000;
        tclk(); test(65000);
        load = 0;

        load = 1;
        in = out - 5;
        tclk(); test(64995);
        load = 0;

        reset = 1;
        tclk(); test(0);
        reset = 0;
    end

endmodule
